﻿#include <iostream>

#define new new( _NORMAL_BLOCK, __FILE__, __LINE__)
#ifdef _DEBUG
#include <crtdbg.h>
#define _CRTDBG_MAP_ALLOC
#endif
template<class T>
class Stack {
private:
    int _size;
    int _pos = -1;
    T* _data;
public:
    Stack(const size_t& _s=10) : _size(_s) {
        _data = new T[_size];
    }

    ~Stack() {
        delete[] _data;
    }

    Stack(const std::initializer_list<T>& init_list) : Stack(init_list.size()) {
        for (const T& elem : init_list)
            _data[++_pos] = elem;
    }

    Stack(const Stack& _st) : _size(_st._size), _pos(_st._pos) {
        delete[] _data;
        _data = new T[_size];
        memcpy(_data, _st._data, _size*sizeof(_data[0]));
    }

    void resize(const size_t size){
        T* data = new T[size];
        memcpy(data, _data, _size * sizeof(_data[0]));
        _size = size;
        delete[] _data;
        _data = data;
    }

    void push_back(const T& _elem) {
        if (_pos < _size - 1)
            _data[++_pos] = _elem;
        else
        {
            resize(_size * 2);
            _data[++_pos] = _elem;
        }
    }

    T pop_back() {
        return _data[_pos--];
    }

    size_t is_empty() {
        return _pos == -1;
    }
};

int main()
{
    _CrtMemState _ms;
    _CrtMemCheckpoint(&_ms);
    Stack<int> st = {1,2,3,4,5};
    Stack<int> st1 = st;
    st1.push_back(10);
    for (size_t i = 0; !st1.is_empty(); ++i)
        std::cout << st1.pop_back() << std::endl;
    _CrtSetReportMode( _CRT_WARN, _CRTDBG_MODE_FILE );
    _CrtSetReportFile( _CRT_WARN, _CRTDBG_FILE_STDOUT );
    _CrtDumpMemoryLeaks();
}
